﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.GameEngine
{
    public class TankFragment
    {
        public int X { get; set; }
        public int Y { get; set; }

        public TankFragment(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
