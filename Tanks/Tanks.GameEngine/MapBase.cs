﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.GameEngine
{
    public class MapBase
    {
        public List<int> Borders = new List<int>(); 
        public void SetBorders(int topleft, int topRight, int bottomLeft, int bottomRight)
        {
            Borders[0] = topleft;
            Borders[1] = topRight;
            Borders[2] = bottomLeft;
            Borders[3] = bottomRight;
        }
        public virtual void DrawBorders(List<int> borders)
        {
            
        }

        public virtual void DrawMap()
        {
            
        }
    }
}
