﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.GameEngine
{
    public class ControlBase
    {
        public ControlActions Action { get; set; }
       
        public virtual ControlActions GetAction()
        {
            return Action;
        }
    }
}
