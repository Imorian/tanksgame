﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tanks.GameEngine
{
    public enum MoveDirection { Left = 1, Right = 2, Up = 3, Down = 4 }
    public enum ControlActions { MoveLeft = 1, MoveRight = 2, MoveUp = 3, MoveDown = 4, Shoot = 5 }
    class GameEngine
    {
        static MoveDirection _direction;
        static MoveDirection _lastDirection;
        private static ControlActions _action;
        private static bool _game_over = false;
        private const int speed = 400;
        static Thread _game = new Thread(GameProcess);
        public static List<TankFragment> MyTank = new List<TankFragment>();
        public MapBase Map = new MapBase();
        public static TankEngine Tank = new TankEngine();
        public static BulletEngine Bullet = new BulletEngine();
        public static ControlBase ControlBase = new ControlBase();
        public void GameProcess(int x, int y, MoveDirection direction,
            int topLeftBorder, int topRightBorder, int bottomLeftBorder, int bottomRightBorder)
        {
            _lastDirection = direction;
            _direction = direction;
            switch (direction)
            {
                    case MoveDirection.Up:
                    _action = ControlActions.MoveUp;
                    break;
                    case MoveDirection.Down:
                    _action = ControlActions.MoveDown;
                    break;
                    case MoveDirection.Right:
                    _action = ControlActions.MoveRight;
                    break;
                    case MoveDirection.Left:
                    _action = ControlActions.MoveLeft;
                    break;
            }
            Tank.TankInit(out MyTank, x, y,direction);
            Map.SetBorders(topLeftBorder, topRightBorder, bottomLeftBorder, bottomRightBorder);
            Map.DrawBorders(Map.Borders);
            Map.DrawMap();
            _game.Start();
            _game.IsBackground = true;
            GameControl();
            _game.Abort();
        }

        public void GameControl()
        {
            while (_game.IsAlive)
            {
                if (_game_over) break;
                if (_game.IsAlive)
                {
                    if (_game_over)
                    {
                        break;
                    }
                    _action = ControlBase.Action;
                    switch (_action)
                    {
                        case ControlActions.MoveUp:
                            _direction = MoveDirection.Up;
                            break;
                        case ControlActions.MoveDown:
                            _direction = MoveDirection.Down;
                            break;
                        case ControlActions.MoveLeft:
                            _direction = MoveDirection.Left;
                            break;
                        case ControlActions.MoveRight:
                            _direction = MoveDirection.Right;
                            break;
                        case ControlActions.Shoot:
                            Tank.MakeShoot(MyTank[1].X, MyTank[1].Y, _direction);
                            break;
                    }
                }
            }
        }
        static void GameProcess()
        {
            while (!_game_over)
            {
                if (_game_over) break;
                Bullet.BulletsMove(Tank.Bullets);
                Tank.TankMove(MyTank, _direction, _lastDirection);
                _lastDirection = _direction;
                Thread.Sleep(speed);
            }
        }
    }
}
