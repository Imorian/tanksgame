﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.GameEngine
{
    class BulletEngine
    {
        public int X { get; set; }
        public int Y { get; set; }
        public MoveDirection Direction { get; set; }
        MapBase _map = new MapBase();

        public BulletEngine()
        {
            
        }
        public BulletEngine(int x, int y, MoveDirection direction)
        {
            X = x;
            Y = y;
            Direction = direction;
        }

        public void BulletsMove(List<BulletEngine> bullets)
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                if (!BulletNearBorder(bullets[i], _map.Borders))
                {
                    BulletErase(bullets[i]);
                    switch (bullets[i].Direction)
                    {
                            case MoveDirection.Up:
                            BulletMoveUp(bullets[i]);
                            break;
                            case MoveDirection.Down:
                            BulletMoveDown(bullets[i]);
                            break;
                            case MoveDirection.Left:
                            BulletMoveLeft(bullets[i]);
                            break;
                            case MoveDirection.Right:
                            BulletMoveRight(bullets[i]);
                            break;
                    }
                    BulletDraw(bullets[i]);
                }
                else
                {
                    bullets.Remove(bullets[i]);
                }
            }
        }

        private void BulletMoveRight(BulletEngine bulletEngine)
        {
            bulletEngine.X = bulletEngine.X + 2;
        }

        private void BulletMoveLeft(BulletEngine bulletEngine)
        {
            bulletEngine.X = bulletEngine.X - 2;
        }

        private void BulletMoveDown(BulletEngine bulletEngine)
        {
            bulletEngine.Y = bulletEngine.Y + 2;
        }

        private void BulletMoveUp(BulletEngine bulletEngine)
        {
            bulletEngine.Y = bulletEngine.Y - 2;
        }

        public virtual void BulletErase(BulletEngine bulletEngine)
        {

        }

        public bool BulletNearBorder(BulletEngine bulletEngine, List<int> borders)
        {
            bool bulletNearBorder = false;
            switch (bulletEngine.Direction)
            {
                case MoveDirection.Up:
                    if (bulletEngine.Y == borders[0] || bulletEngine.Y - 1 == borders[0])
                    {
                        bulletNearBorder = true;
                    }
                    break;
                case MoveDirection.Down:
                    if (bulletEngine.Y == borders[3] || bulletEngine.Y + 1 == borders[3])
                    {
                        bulletNearBorder = true;
                    }
                    break;
                case MoveDirection.Left:
                    if (bulletEngine.X == borders[0] || bulletEngine.X - 1 == borders[0])
                    {
                        bulletNearBorder = true;
                    }
                    break;
                case MoveDirection.Right:
                    if (bulletEngine.X == borders[3] || bulletEngine.X + 1 == borders[3])
                    {
                        bulletNearBorder = true;
                    }
                    break;
            }
            return bulletNearBorder;

        }

        public virtual void BulletDraw(BulletEngine bulletEngine)
        {

        }
    }
}
