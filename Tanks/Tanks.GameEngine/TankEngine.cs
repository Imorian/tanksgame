﻿using System.Collections.Generic;

namespace Tanks.GameEngine
{
    internal class TankEngine
    {
        public List<TankFragment> MyTank = new List<TankFragment>();
        public List<BulletEngine> Bullets = new List<BulletEngine>(); 
        public bool ShootEnable { get; set; }

        public void TankInit(out List<TankFragment> tank, int x, int y, MoveDirection direction)
        {
            tank = TankCreate(x, y, direction);
            TankDraw(tank, direction);
        }

        public List<TankFragment> TankCreate(int x, int y, MoveDirection direction)
        {
            var tank = new List<TankFragment>();
            switch (direction)
            {
                case MoveDirection.Up:
                    tank[0].X = x;
                    tank[0].Y = y;
                    tank[1].X = tank[0].X + 1;
                    tank[1].Y = tank[0].Y;
                    tank[2].X = tank[0].X + 2;
                    tank[2].Y = tank[0].Y;
                    tank[3].X = tank[0].X;
                    tank[3].Y = tank[0].Y + 1;
                    tank[4].X = tank[0].X + 1;
                    tank[4].Y = tank[0].Y + 1;
                    tank[5].X = tank[0].X + 2;
                    tank[5].Y = tank[0].Y + 1;
                    tank[6].X = tank[0].X;
                    tank[6].Y = tank[0].Y + 2;
                    tank[7].X = tank[0].X + 1;
                    tank[7].Y = tank[0].Y + 2;
                    tank[8].X = tank[0].X + 2;
                    tank[8].Y = tank[0].Y + 2;
                    break;
                case MoveDirection.Down:
                    tank[0].X = x;
                    tank[0].Y = y;
                    tank[1].X = tank[0].X - 1;
                    tank[1].Y = tank[0].Y;
                    tank[2].X = tank[0].X - 2;
                    tank[2].Y = tank[0].Y;
                    tank[3].X = tank[0].X;
                    tank[3].Y = tank[0].Y - 1;
                    tank[4].X = tank[0].X - 1;
                    tank[4].Y = tank[0].Y - 1;
                    tank[5].X = tank[0].X - 2;
                    tank[5].Y = tank[0].Y - 1;
                    tank[6].X = tank[0].X;
                    tank[6].Y = tank[0].Y - 2;
                    tank[7].X = tank[0].X - 1;
                    tank[7].Y = tank[0].Y - 2;
                    tank[8].X = tank[0].X - 2;
                    tank[8].Y = tank[0].Y - 2;
                    break;
                case MoveDirection.Left:
                    tank[0].X = x;
                    tank[0].Y = y;
                    tank[1].X = tank[0].X;
                    tank[1].Y = tank[0].Y - 1;
                    tank[2].X = tank[0].X;
                    tank[2].Y = tank[0].Y - 2;
                    tank[3].X = tank[0].X + 1;
                    tank[3].Y = tank[0].Y;
                    tank[4].X = tank[0].X + 1;
                    tank[4].Y = tank[0].Y - 1;
                    tank[5].X = tank[0].X + 1;
                    tank[5].Y = tank[0].Y - 2;
                    tank[6].X = tank[0].X + 2;
                    tank[6].Y = tank[0].Y;
                    tank[7].X = tank[0].X + 2;
                    tank[7].Y = tank[0].Y - 1;
                    tank[8].X = tank[0].X + 2;
                    tank[8].Y = tank[0].Y - 2;
                    break;
                case MoveDirection.Right:
                    tank[0].X = x;
                    tank[0].Y = y;
                    tank[1].X = tank[0].X;
                    tank[1].Y = tank[0].Y + 1;
                    tank[2].X = tank[0].X;
                    tank[2].Y = tank[0].Y + 2;
                    tank[3].X = tank[0].X - 1;
                    tank[3].Y = tank[0].Y;
                    tank[4].X = tank[0].X - 1;
                    tank[4].Y = tank[0].Y + 1;
                    tank[5].X = tank[0].X - 1;
                    tank[5].Y = tank[0].Y + 2;
                    tank[6].X = tank[0].X - 2;
                    tank[6].Y = tank[0].Y;
                    tank[7].X = tank[0].X - 2;
                    tank[7].Y = tank[0].Y + 1;
                    tank[8].X = tank[0].X - 2;
                    tank[8].Y = tank[0].Y + 2;
                    break;
            }
            return tank;
        }

        public void TankMove(List<TankFragment> tank, MoveDirection direction, MoveDirection lastDirection)
        {
            TankErase(tank);
            TankTurn(tank, direction, lastDirection);
            switch (direction)
            {
                    case MoveDirection.Up:
                    TankMoveUp(tank);
                    break;
                    case MoveDirection.Down:
                    TankMoveDown(tank);
                    break;
                    case MoveDirection.Left:
                    TankMoveLeft(tank);
                    break;
                    case MoveDirection.Right:
                    TankMoveRight(tank);
                    break;
            }
            TankDraw(tank, direction);
        }

        private void TankMoveRight(List<TankFragment> tank)
        {
            tank[0].X = tank[0].X + 1;
            tank[1].X = tank[1].X + 1;
            tank[2].X = tank[2].X + 1;
            tank[3].X = tank[3].X + 1;
            tank[4].X = tank[4].X + 1;
            tank[5].X = tank[5].X + 1;
            tank[6].X = tank[6].X + 1;
            tank[7].X = tank[7].X + 1;
            tank[8].X = tank[8].X + 1;
        }

        private void TankMoveLeft(List<TankFragment> tank)
        {
            tank[0].X = tank[0].X - 1;
            tank[1].X = tank[1].X - 1;
            tank[2].X = tank[2].X - 1;
            tank[3].X = tank[3].X - 1;
            tank[4].X = tank[4].X - 1;
            tank[5].X = tank[5].X - 1;
            tank[6].X = tank[6].X - 1;
            tank[7].X = tank[7].X - 1;
            tank[8].X = tank[8].X - 1;
        }

        private void TankMoveDown(List<TankFragment> tank)
        {
            tank[0].Y = tank[0].Y + 1;
            tank[1].Y = tank[1].Y + 1;
            tank[2].Y = tank[2].Y + 1;
            tank[3].Y = tank[3].Y + 1;
            tank[4].Y = tank[4].Y + 1;
            tank[5].Y = tank[5].Y + 1;
            tank[6].Y = tank[6].Y + 1;
            tank[7].Y = tank[7].Y + 1;
            tank[8].Y = tank[8].Y + 1;
        }

        private void TankMoveUp(List<TankFragment> tank)
        {
            tank[0].Y = tank[0].Y - 1;
            tank[1].Y = tank[1].Y - 1;
            tank[2].Y = tank[2].Y - 1;
            tank[3].Y = tank[3].Y - 1;
            tank[4].Y = tank[4].Y - 1;
            tank[5].Y = tank[5].Y - 1;
            tank[6].Y = tank[6].Y - 1;
            tank[7].Y = tank[7].Y - 1;
            tank[8].Y = tank[8].Y - 1;
        }

        public void Shoot(int x, int y, MoveDirection direction)
        {
            if (ShootEnable)
            {
                MakeShoot(x, y, direction);
                ShootEnable = false;
            }
        }

        public void MakeShoot(int x, int y, MoveDirection direction)
        {
            BulletEngine lBulletEngine = new BulletEngine(x, y, direction);
            Bullets.Add(lBulletEngine);
        }

        public virtual void TankDraw(List<TankFragment> tank, MoveDirection direction)
        {
        }

        public virtual void TankTurn(List<TankFragment> tank, MoveDirection direction, MoveDirection lastDirection)
        {
            switch (direction)
            {
                case MoveDirection.Left:
                    switch (lastDirection)
                    {
                        case MoveDirection.Up:
                            #region turn_up->left
                            tank[0].X = tank[6].X;
                            tank[0].Y = tank[6].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y - 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y - 2;
                            tank[3].X = tank[0].X + 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X + 1;
                            tank[5].Y = tank[0].Y - 2;
                            tank[6].X = tank[0].X + 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X + 2;
                            tank[7].Y = tank[0].Y - 1;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                        case MoveDirection.Right:
                            #region turn_right->left
                            tank[0].X = tank[8].X;
                            tank[0].Y = tank[8].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y - 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y - 2;
                            tank[3].X = tank[0].X + 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X + 1;
                            tank[5].Y = tank[0].Y - 2;
                            tank[6].X = tank[0].X + 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X + 2;
                            tank[7].Y = tank[0].Y - 1;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                        case MoveDirection.Down:
                            #region turn_down->left
                            tank[0].X = tank[2].X;
                            tank[0].Y = tank[2].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y - 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y - 2;
                            tank[3].X = tank[0].X + 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X + 1;
                            tank[5].Y = tank[0].Y - 2;
                            tank[6].X = tank[0].X + 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X + 2;
                            tank[7].Y = tank[0].Y - 1;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                    }
                    break;
                case MoveDirection.Right:
                    switch (lastDirection)
                    {
                        case MoveDirection.Left:
                            #region turn_left->right
                            tank[0].X = tank[8].X;
                            tank[0].Y = tank[8].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y + 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y + 2;
                            tank[3].X = tank[0].X - 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X - 1;
                            tank[5].Y = tank[0].Y + 2;
                            tank[6].X = tank[0].X - 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X - 2;
                            tank[7].Y = tank[0].Y + 1;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                        case MoveDirection.Up:
                            #region turn_up->right
                            tank[0].X = tank[2].X;
                            tank[0].Y = tank[2].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y + 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y + 2;
                            tank[3].X = tank[0].X - 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X - 1;
                            tank[5].Y = tank[0].Y + 2;
                            tank[6].X = tank[0].X - 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X - 2;
                            tank[7].Y = tank[0].Y + 1;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                        case MoveDirection.Down:
                            #region turn_down->right
                            tank[0].X = tank[6].X;
                            tank[0].Y = tank[6].Y;
                            tank[1].X = tank[0].X;
                            tank[1].Y = tank[0].Y + 1;
                            tank[2].X = tank[0].X;
                            tank[2].Y = tank[0].Y + 2;
                            tank[3].X = tank[0].X - 1;
                            tank[3].Y = tank[0].Y;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X - 1;
                            tank[5].Y = tank[0].Y + 2;
                            tank[6].X = tank[0].X - 2;
                            tank[6].Y = tank[0].Y;
                            tank[7].X = tank[0].X - 2;
                            tank[7].Y = tank[0].Y + 1;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                    }
                    break;
                case MoveDirection.Up:
                    switch (lastDirection)
                    {
                        case MoveDirection.Left:
                            #region turn_left->up
                            tank[0].X = tank[2].X;
                            tank[0].Y = tank[2].Y;
                            tank[1].X = tank[0].X + 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X + 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y + 1;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X + 2;
                            tank[5].Y = tank[0].Y + 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y + 2;
                            tank[7].X = tank[0].X + 1;
                            tank[7].Y = tank[0].Y + 2;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                        case MoveDirection.Right:
                            #region turn_right->up
                            tank[0].X = tank[6].X;
                            tank[0].Y = tank[6].Y;
                            tank[1].X = tank[0].X + 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X + 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y + 1;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X + 2;
                            tank[5].Y = tank[0].Y + 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y + 2;
                            tank[7].X = tank[0].X + 1;
                            tank[7].Y = tank[0].Y + 2;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                        case MoveDirection.Down:
                            #region turn_down->up
                            tank[0].X = tank[8].X;
                            tank[0].Y = tank[8].Y;
                            tank[1].X = tank[0].X + 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X + 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y + 1;
                            tank[4].X = tank[0].X + 1;
                            tank[4].Y = tank[0].Y + 1;
                            tank[5].X = tank[0].X + 2;
                            tank[5].Y = tank[0].Y + 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y + 2;
                            tank[7].X = tank[0].X + 1;
                            tank[7].Y = tank[0].Y + 2;
                            tank[8].X = tank[0].X + 2;
                            tank[8].Y = tank[0].Y + 2;
                            #endregion
                            break;
                    }
                    break;
                case MoveDirection.Down:
                    switch (lastDirection)
                    {
                        case MoveDirection.Left:
                            #region turn_left->down
                            tank[0].X = tank[6].X;
                            tank[0].Y = tank[6].Y;
                            tank[1].X = tank[0].X - 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X - 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y - 1;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X - 2;
                            tank[5].Y = tank[0].Y - 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y - 2;
                            tank[7].X = tank[0].X - 1;
                            tank[7].Y = tank[0].Y - 2;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                        case MoveDirection.Right:
                            #region turn_right->down
                            tank[0].X = tank[2].X;
                            tank[0].Y = tank[2].Y;
                            tank[1].X = tank[0].X - 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X - 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y - 1;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X - 2;
                            tank[5].Y = tank[0].Y - 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y - 2;
                            tank[7].X = tank[0].X - 1;
                            tank[7].Y = tank[0].Y - 2;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                        case MoveDirection.Up:
                            #region turn_left->down
                            tank[0].X = tank[8].X;
                            tank[0].Y = tank[8].Y;
                            tank[1].X = tank[0].X - 1;
                            tank[1].Y = tank[0].Y;
                            tank[2].X = tank[0].X - 2;
                            tank[2].Y = tank[0].Y;
                            tank[3].X = tank[0].X;
                            tank[3].Y = tank[0].Y - 1;
                            tank[4].X = tank[0].X - 1;
                            tank[4].Y = tank[0].Y - 1;
                            tank[5].X = tank[0].X - 2;
                            tank[5].Y = tank[0].Y - 1;
                            tank[6].X = tank[0].X;
                            tank[6].Y = tank[0].Y - 2;
                            tank[7].X = tank[0].X - 1;
                            tank[7].Y = tank[0].Y - 2;
                            tank[8].X = tank[0].X - 2;
                            tank[8].Y = tank[0].Y - 2;
                            #endregion
                            break;
                    }
                    break;
            }
        }

        public virtual void TankErase(List<TankFragment> tank)
        {
        }
    }
}